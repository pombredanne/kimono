//
//  KMBrowserTabController.m
//  Kimono
//
//  Created by James Dumay on 25/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMBrowserTabController.h"

@implementation KMBrowserTabController

@synthesize webView = _webView;
@synthesize errorViewController = _errorViewController;
@synthesize pageUpdater = _pageUpdater;
@synthesize pageIcon = _pageIcon;
@synthesize pageTitle = _pageTitle;
@synthesize pageLocation = _pageLocation;

- (id)initWithPageUpdater:(id<KMPageUpdater>)pageUpdater
{
    self = [super init];
    _pageUpdater = pageUpdater;
    [[_errorViewController view] setHidden:YES];
    return self;
}

- (NSString *)nibName
{
    return @"KMBrowserTabController";
}

-(void)loadURL:(NSURL*)url
{
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [[_webView mainFrame] loadRequest:request];
}

- (NSString *)pageTitle
{
    NSString *title = [_webView mainFrameTitle];
    
    if (title == nil || [title length] == 0)
    {
        title = @"New Tab";
    }
    
    return title;
}

- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame
{
    NSLog(@"Failed to load with error");
    [_pageUpdater update:self];
}

- (void)webView:(WebView *)sender didReceiveTitle:(NSString *)title forFrame:(WebFrame *)frame
{
    if (frame == [sender mainFrame])
    {
        _pageTitle = title;
        [_pageUpdater update:self];
    }
}

- (void)webView:(WebView *)sender didReceiveIcon:(NSImage *)image forFrame:(WebFrame *)frame
{
    if (frame == [sender mainFrame])
    {
        _pageIcon = image;
    }
}

- (void)webView:(WebView *)sender didStartProvisionalLoadForFrame:(WebFrame *)frame
{
    // Only report feedback for the main frame.
    if (frame == [sender mainFrame])
    {
        _pageLocation = [[[frame provisionalDataSource] request] URL];
        [self hideErrorView];
    }
    [_pageUpdater update:self];
}

-(NSURLRequest *)webView:(WebView *)sender
                resource:(id)identifier
         willSendRequest:(NSURLRequest *)request
        redirectResponse:(NSURLResponse *)redirectResponse
          fromDataSource:(WebDataSource *)dataSource
{
    [_pageUpdater update:self];
    return request;
}

- (void)webView:(WebView *)sender didFailProvisionalLoadWithError:(NSError *)error forFrame:(WebFrame *)frame
{
    if (frame == [sender mainFrame])
    {
        NSDictionary *userInfo = [error userInfo];
        
        NSString *failureDescription = (NSString*)[userInfo objectForKey:@"NSLocalizedDescription"];
//        NSLog(@"failureDescription: <%@>", failureDescription);
        NSURL *urlFailedToLoad = (NSURL*)[userInfo objectForKey:@"NSErrorFailingURLKey"];
//        NSLog(@"urlFailedToLoad: <%@>", urlFailedToLoad); 
        
//        [_pageU pageRequestedFailed:urlFailedToLoad withReason:failureDescription];
        
        [_errorViewController urlCouldNotBeLoaded:urlFailedToLoad withMessage:failureDescription];
        [self showErrorView];
        
//        for (int i = 0; i < [userInfo count]; i++) {
//            NSString *key = [[userInfo allKeys] objectAtIndex:i];
//            NSString *value = [[userInfo allValues] objectAtIndex:i];
//            NSLog(@"%@: %@", key, value);
//        }
    }
    [_pageUpdater update:self];
}

-(void)webView:(WebView *)sender resource:(id)identifier didFinishLoadingFromDataSource:(WebDataSource *)dataSource
{
    [_pageUpdater update:self];
}

- (void)showErrorView;
{
    if ([[[self view] subviews] containsObject:_webView])
    {
        [[_errorViewController view] setFrame:[[self view] frame]];
        [[self view] replaceSubview:_webView with:[_errorViewController view]];
    }
}

- (void)hideErrorView
{
    if ([[[self view] subviews] containsObject:[_errorViewController view]])
    {
        [_webView setFrame:[[self view] frame]];
        [[self view] replaceSubview:[_errorViewController view] with:_webView];
    }
}

- (void)reload
{
    [[_webView webFrame] reload];
    [_pageUpdater update:self];
}

- (void)stopLoad
{
    [[_webView webFrame] stopLoading];
    [_pageUpdater update:self];
}

- (NSString*)title
{
    NSString *title = [_webView mainFrameTitle];
    
    if (title == nil || [title length] == 0)
    {
        title = @"New Tab";
    }
    
    return title;
}

- (NSURL*)location
{
    return _pageLocation;
}

- (NSImage*)icon
{
    if (_pageIcon == nil)
    {
        NSSize size = NSMakeSize(22, 22);
        _pageIcon = [[NSImage alloc] initWithSize:size];
    }
    return _pageIcon;
}

- (double) progress
{
    return [_webView estimatedProgress] * 100;
}

- (BOOL)loading
{
    return [_webView isLoading];
}

@end
