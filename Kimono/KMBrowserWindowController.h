//
//  NSBrowserWindowController.h
//  Kimono
//
//  Created by James Dumay on 28/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KMBrowserTabController.h"
#import "KMSmartbar.h"
#import "KMBrowserTab.h"
#import "KMPageUpdater.h"
#import "KMBrowserToolbar.h"

@class KMBrowserTabController;

@interface KMBrowserWindowController : NSWindowController <KMPageUpdater>

@property (strong) IBOutlet KMBrowserToolbar *toolbar;
@property (strong) IBOutlet KMSmartbar *smartBar;
@property (strong) IBOutlet NSToolbarItem *smartBarToolbarItem;
@property (strong) IBOutlet NSView *innerView;
@property (strong) IBOutlet NSPopover *tabsPopover;
@property (strong) IBOutlet NSSearchField *tabSearchField;
@property (strong) IBOutlet NSArrayController *tabsController;
@property (strong) IBOutlet NSSegmentedControl *showTabListButton;
@property (strong) IBOutlet NSTableView *tabTableView;
@property (strong) IBOutlet NSView *view;

@property (strong) NSMutableArray *tabControllers;
@property (nonatomic, strong, setter = setCurrentTab:) KMBrowserTabController *currentTab;

- (IBAction)showActiveTabs:(NSSegmentedControl *)sender;
- (IBAction)goBackOrForward:(NSSegmentedControl *)sender;
- (IBAction)smartBarActivated:(NSTextField *)sender;
- (IBAction)searchTabs:(id)sender;
- (IBAction)closeTab:(id)sender;

- (KMBrowserTabController *)createNewTab:(NSURL *)url;

- (void)focusSmartbar;

- (void)reload;

- (void)stopLoad;

- (void)showTabs;

- (void)closeCurrentTab;

- (void)closePopovers;

@end
