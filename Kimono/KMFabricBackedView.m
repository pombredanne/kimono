//
//  KMFabricBackedView.m
//  Kimono
//
//  Created by James Dumay on 25/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMFabricBackedView.h"

@implementation KMFabricBackedView

- (void)drawRect:(NSRect)dirtyRect
{   
    [[NSGraphicsContext currentContext] saveGraphicsState];
    
    // Change the pattern phase.
    [[NSGraphicsContext currentContext] setPatternPhase:NSMakePoint(0,[self frame].size.height)];
    
    // Stick the image in a color and fill the view with that color.
    
    NSBundle *bundle = [NSBundle bundleWithIdentifier:@"com.apple.AppKit"];
    NSURL * url = [bundle URLForResource:@"NSTexturedFullScreenBackgroundColor" withExtension:@"png"];
    
    NSImage *anImage = [[NSImage alloc] initWithContentsOfURL:url];
    [[NSColor colorWithPatternImage:anImage] set];
    
    //Use Kimono blue instead of fabric?
    
//    [[NSColor colorWithCalibratedRed:0.39 green:0.70 blue:0.89 alpha:1.00] set];
    NSRectFill([self bounds]);
    
    // Restore the original graphics state.
    [[NSGraphicsContext currentContext] restoreGraphicsState];
    
    [super drawRect:dirtyRect];
}

@end
